#include "Draw.h"

#define ERASING_SIZE 4
const int NORMAL_LINE = 1;
const int HELPING_LINE = 2;



// used for storing current line
Line new_line;

// Stroke_M = std::map<int, Line>
// active lines, each line has its index
// index nessesary for erasing lines
Stroke_M strokes;

// Stroke_V = std::vector<Line>
// simplified lines
Stroke_V segm_strokes;

// 2D int array of pixels
// stores line indexes in pixels, where they are drawn 
// then we can easily retrive line, if there is any, from specific position
// used for erasing
// todo add array insted of ints
std::vector<std::vector<int>> line_image_map(WIDTH, std::vector<int>(HEIGHT, 0));

// Line_C : line with its cooficients (a, b, c)
// stores help lines
std::vector<Line_C>  help_lines;

//current help line
Line_C help_line;

// how many times user clicks
// 2 clicks means, that we can store a line
short help_line_counter;

// indexed arrays of points
// index means the order of segmentation
// point array stores pixels, that has to be colored 
// creates filled shapes
std::map<int, std::vector<Point>> colored_area;



int line_amount = 1;

bool first_segmentation = true;

// ----- undo structures -------
int undo_last_line_type = NORMAL_LINE;

Stroke_V undo_step_segm_strokes;
std::vector<std::vector<int>> undo_line_image_map;
Stroke_M undo_step_strokes;
std::map<int, std::vector<Point>> undo_colored_area;
int undo_line_amount = 1;

/// <summary>
/// Computes line's length
/// </summary>
float line_len(Line line) {
  return sqrt(pow(line.x1 - line.x2, 2) + pow(line.y1 - line.y2, 2));
}

/// <summary>
/// Init function
/// </summary>
void must_init(bool test, const char* description)
{
  if (test) return;
  printf("couldn't initialize %s\n", description);
  exit(1);
}

/// <summary>
///  Draw informarion for the user
/// </summary>
void window_info(ALLEGRO_FONT* font) {
  al_draw_text(font, al_map_rgb(20, 20, 20), 10, 10, 0, "Info:");
  al_draw_text(font, al_map_rgb(20, 20, 20), 10, 20, 0, "Before segmentation area must be closed");
  al_draw_text(font, al_map_rgb(20, 20, 20), 10, 30, 0, "To Erase active(red line) hold E");
  al_draw_text(font, al_map_rgb(20, 20, 20), 10, 40, 0, "To Undo last segmentation press Z");
  al_draw_text(font, al_map_rgb(20, 20, 20), 10, 50, 0, "To Delete everything press D");
  al_draw_text(font, al_map_rgb(20, 20, 20), 10, 60, 0, "To create Help line press LShift and click to add end points of line");
  al_draw_text(font, al_map_rgb(20, 20, 20), 10, 70, 0, "To segment press S");

}

///// <summary>
///// Auxiliary funciton for check_borders
///// </summary>
//int clamp(int x, int size) {
//  if (x < 0)
//    return 0;
//
//  if (x >= size)
//    return size - 1;
//
//  return x;
//}

/// <summary>
/// Checks if line is in the canvas, if not, clamps overlaping values
/// </summary>
void check_borders(Line &line) {
  line.x1 = clamp_val(line.x1, WIDTH);
  line.y1 = clamp_val(line.y1, HEIGHT);
  line.x2 = clamp_val(line.x2, WIDTH);
  line.y2 = clamp_val(line.y2, HEIGHT);
}


/// <summary>
/// Draws line with Bresenham algorithm.
/// Funciton from: https://www.geeksforgeeks.org/bresenhams-line-generation-algorithm/
/// </summary>
void plotPixel(std::vector<std::vector<int>>& line_image, int x1, int y1, int x2, int y2, int dx, int dy, int decide, int tag) {
  // pk is initial decision making parameter
  // Note:x1&y1,x2&y2, dx&dy values are interchanged
  // and passed in plotPixel function so
  // it can handle both cases when m>1 & m<1
  int pk = 2 * dy - dx;
  for (int i = 0; i <= dx; i++) {
    // checking either to decrement or increment the
    // value if we have to plot from (0,100) to (100,0)
    x1 < x2 ? x1++ : x1--;
    if (pk < 0) {
      // decision value will decide to plot
      // either  x1 or y1 in x's position
      if (decide == 0) {
        line_image[clamp_val(x1, WIDTH)][clamp_val(y1, HEIGHT)] = tag;
        pk = pk + 2 * dy;

      }
      else {
        //(y1,x1) is passed in xt
        line_image[clamp_val(y1, WIDTH)][clamp_val(x1, HEIGHT)] = tag;
        pk = pk + 2 * dy;
      }
    }
    else {
      y1 < y2 ? y1++ : y1--;

      if (decide == 0) {
        line_image[clamp_val(x1, WIDTH)][clamp_val(y1, HEIGHT)] = tag;
      }
      else {
        line_image[clamp_val(y1, WIDTH)][clamp_val(x1, HEIGHT)] = tag;
      }
      pk = pk + 2 * dy - 2 * dx;
    }
  }
}

/// <summary>
/// Draw line into the line_image_map with Bresenham algorithm
/// </summary>
void add_line_to_map(std::vector<std::vector<int>> &line_image, Line l, int tag) {
  int dx = abs(l.x2 - l.x1);
  int dy = abs(l.y2 - l.y1);
  if (dx > dy) {
    plotPixel(line_image, l.x1, l.y1, l.x2, l.y2, dx, dy, 0, tag);
  } else {
    plotPixel(line_image, l.y1, l.x1, l.y2, l.x2, dy, dx, 1, tag);
  }
}

/// <summary>
/// Erases line near the point [x, y]
/// </summary>
void erase_lines(int x, int y) {

  //ERASING_SIZE
  for (int i = x - ERASING_SIZE; i <= x + ERASING_SIZE; i++) {
    for (int j = y - ERASING_SIZE; j <= y + ERASING_SIZE; j++) {
      if (i >= 0 && i < WIDTH && j >= 0 && j < HEIGHT) {
        int tag = line_image_map[i][j];
        if (tag) {
          strokes.erase(tag);
        }
      }
    }
  }
}

/// <summary>
/// Calls stroke simplifier, saves all structures to the undo structure, clears canvas
/// </summary>
void segment(ALLEGRO_FONT* font) {
  auto start = std::chrono::high_resolution_clock::now();
  first_segmentation = false;
  al_draw_text(font, al_map_rgb(255, 0, 0), 10, 85, 0, "Segmenting..");
  al_flip_display();
  undo_step_strokes = strokes;
  undo_step_segm_strokes = segm_strokes;
  undo_line_image_map = line_image_map;
  undo_line_amount = line_amount;
  undo_colored_area = colored_area;
  std::vector<Line_C>::iterator itr2;
  bool res = false;

  std::vector<Point> new_area;
  Stroke_V new_strokes = segment_drawing(strokes, help_lines, new_area);
  colored_area.emplace(colored_area.size(), new_area);
  segm_strokes.insert(segm_strokes.end(), new_strokes.begin(), new_strokes.end());
  strokes.clear();
  
  help_lines.clear();
  help_line_counter = 0;

  //set image map to zero
  for (int i = 0; i < WIDTH; i++) {
    for (int j = 0; j < HEIGHT; j++) {
      line_image_map[i][j] = 0;
    }
  }

  auto stop = std::chrono::high_resolution_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
  std::cout << "Done in " << duration.count() << "\n";
  std::cout << "segm_strokes size " << segm_strokes.size() << "\n";

}

/// <summary>
/// Main loop of the program
/// Initializes windows, waits for events, draws everything
/// Creates new strokes
/// </summary>
void main_loop() {
  must_init(al_init(), "allegro");
  must_init(al_install_keyboard(), "keyboard");
  must_init(al_install_mouse(), "mouse");
  ALLEGRO_TIMER* timer = al_create_timer(1.0 / FPS);
  must_init(timer, "timer");

  ALLEGRO_EVENT_QUEUE* queue = al_create_event_queue();
  must_init(queue, "queue");

  al_set_new_display_option(ALLEGRO_SAMPLE_BUFFERS, 1, ALLEGRO_SUGGEST);
  al_set_new_display_option(ALLEGRO_SAMPLES, 8, ALLEGRO_SUGGEST);
  al_set_new_bitmap_flags(ALLEGRO_MIN_LINEAR | ALLEGRO_MAG_LINEAR);

  ALLEGRO_DISPLAY* disp = al_create_display(WIDTH, HEIGHT);
  must_init(disp, "display");

  ALLEGRO_FONT* font = al_create_builtin_font();
  must_init(font, "font");

  must_init(al_init_primitives_addon(), "primitives");

  al_register_event_source(queue, al_get_keyboard_event_source());
  al_register_event_source(queue, al_get_display_event_source(disp));
  al_register_event_source(queue, al_get_timer_event_source(timer));
  al_register_event_source(queue, al_get_mouse_event_source());


  al_start_timer(timer);
  al_clear_to_color(al_map_rgb(255, 255, 255));

  
  ALLEGRO_EVENT event;

  bool done = false;
  bool redraw = true;
  bool mouse_pressed = false;
  bool first_coord_mouse = true;
  bool segmentation = false;
  bool lshift = false;
  bool erasing = false;


  while (1){
    al_wait_for_event(queue, &event);

    switch (event.type)    {

    

    case ALLEGRO_EVENT_TIMER:
      redraw = true;
      break;
    case ALLEGRO_EVENT_KEY_DOWN:
      switch (event.keyboard.keycode) {
      case ALLEGRO_KEY_S:
        segment(font);
        line_amount = 1;

        break;
      case ALLEGRO_KEY_D:
        strokes.clear();
        segm_strokes.clear();
        help_line_counter = 0;
        help_lines.clear();
        for (int i = 0; i < WIDTH; i++) {
          for (int j = 0; j < HEIGHT; j++) {
            line_image_map[i][j] = 0;
          //  undo_line_image_map[i][j] = 0;
          }
        }
        line_amount = 1;

        undo_step_strokes.clear();
        undo_step_segm_strokes.clear();
        colored_area.clear();
        undo_colored_area.clear();
        undo_line_amount = 1;
        break;
      case ALLEGRO_KEY_Z:
        if (first_segmentation) {
          strokes.clear();
          line_amount = 1;
          segm_strokes.clear();
          for (int i = 0; i < WIDTH; i++) {
            for (int j = 0; j < HEIGHT; j++) {
              line_image_map[i][j] = 0;
            }
          }
        }
        else {
          strokes = undo_step_strokes;
          line_amount = undo_line_amount;
          segm_strokes = undo_step_segm_strokes;
          line_image_map = undo_line_image_map;
          colored_area = undo_colored_area;
        }
        break;
      case ALLEGRO_KEY_ESCAPE:
        done = true;
        break;
      case ALLEGRO_KEY_LSHIFT:
        lshift = true;
        break;
      
      }
      break;
    case ALLEGRO_EVENT_KEY_CHAR:
      if (event.keyboard.keycode == ALLEGRO_KEY_E) {
        erase_lines(new_line.x2, new_line.y2);
        erasing = true;
      }
      break;
    case ALLEGRO_EVENT_KEY_UP:
    {
      if (event.keyboard.keycode == ALLEGRO_KEY_E) {
        erasing = false;
      }
      else if (event.keyboard.keycode == ALLEGRO_KEY_LSHIFT) {
        lshift = false;
      }
      break;
    }
    case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
      if (first_coord_mouse) {
        first_coord_mouse = false;
        
        new_line.x1 = event.mouse.x;
        new_line.y1 = event.mouse.y;
       
      }
      mouse_pressed = true;
      al_hide_mouse_cursor(disp);
      break;

    case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
      mouse_pressed = false;
      al_show_mouse_cursor(disp);
      first_coord_mouse = true;
      if (lshift) {
        if (help_line_counter == 0) {
          help_line.x1 = event.mouse.x;
          help_line.y1 = event.mouse.y;
        }
        else if (help_line_counter == 1) {
          help_line.x2 = event.mouse.x;
          help_line.y2 = event.mouse.y;
        }
        help_line_counter++;
      }

      break;

    case ALLEGRO_EVENT_MOUSE_AXES:
      new_line.x2 = event.mouse.x;
      new_line.y2 = event.mouse.y;
      break;
    case ALLEGRO_EVENT_DISPLAY_CLOSE:
      done = true;
      break;
    }

    if (done) {
      break;
    }
   /**
   * Redraws everything
   * Saves new lines
   */
    if (redraw && al_is_event_queue_empty(queue)) {
      
      al_clear_to_color(al_map_rgb(255, 255, 255));

      // window info
      window_info(font);
      if (erasing) {
        al_draw_text(font, al_map_rgb(200, 0, 0), 10, 85, 0, "Erasing..");
      }
      if (lshift) {
        al_draw_text(font, al_map_rgb(0, 180, 0), 10, 85, 0, "Adding help line");
      }

      // draw areas
      std::map<int, std::vector<Point>>::iterator itr_area;
      for (itr_area = colored_area.begin(); itr_area != colored_area.end(); ++itr_area) {
        int order = itr_area->first;
        ALLEGRO_COLOR back = al_map_rgb(30, 90, 30);

        if (order == 0) { //first, diffrent color
          back = al_map_rgb(60, 60, 100);
        }
        back.a = 0.5;
        for (int i = 0; i < itr_area->second.size(); i++) {
          al_draw_pixel(itr_area->second[i].x, itr_area->second[i].y, back);
        }

      }

      //draw helping lines
      std::vector<Line_C>::iterator itr2;
      for (itr2 = help_lines.begin(); itr2 != help_lines.end(); ++itr2) {
        Line_C line = *itr2;
        al_draw_line(line.x1, line.y1, line.x2, line.y2, al_map_rgb(0, 255, 0), 2);
      }
      
      //draw segmented strokes
      for (int i = 0; i < segm_strokes.size(); i++) {
        Line line = segm_strokes[i];
        al_draw_line(line.x1, line.y1, line.x2, line.y2, al_map_rgb(150, 150, 150), 2);
      }

      //draw active strokes
      Stroke_M::iterator itr;
      for (itr = strokes.begin(); itr != strokes.end(); ++itr) {
        Line line = itr->second;
        al_draw_line(line.x1, line.y1, line.x2, line.y2, al_map_rgb(255, 0, 0), 2);
      }

      
      
      // create new line
      if (mouse_pressed && line_len(new_line) > MIN_LINE_LEN) {

        check_borders(new_line);
        al_draw_line(new_line.x1, new_line.y1, new_line.x2, new_line.y2, al_map_rgb(255, 0, 0), 2);
        
        add_line_to_map(line_image_map, new_line, line_amount);
        strokes[line_amount] = new_line;

        new_line.x1 = new_line.x2;
        new_line.y1 = new_line.y2;
        line_amount++;
      }
      //create help line and store its cooficients
      if (help_line_counter >= 2) { 

        help_line.b = (float)help_line.x1 - (float)help_line.x2;
        help_line.a = -((float)help_line.y1 - (float)help_line.y2);
        help_line.c = -(help_line.a * (float)help_line.x1 + help_line.b * (float)help_line.y1);
        help_lines.push_back(help_line);
        help_line_counter = 0;

      }
      
      al_flip_display();
      redraw = false;
    }
  }

  al_destroy_font(font);
  al_destroy_display(disp);
  al_destroy_timer(timer);
  al_destroy_event_queue(queue);

}


int main(){

  main_loop();

  return 0;
}
