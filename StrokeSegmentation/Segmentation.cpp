#include "Segmentation.h"
#include <string>
#include "Image.h"

// terminal capacity
#define K 2 
// max capacity between 2 similar nodes
#define CAP 15

#define RED  RGB(1,0,0)
#define BLUE RGB(0,0,1)
#define WHITE RGB(1,1,1)
#define BLACK RGB(0,0,0)

typedef GridGraph_2D_4C<short, short, int> Grid;
//TODO handle 2 area objects and more


/// <summary>
/// Draws line with Bresenham algorithm.
/// Funciton from: https://www.geeksforgeeks.org/bresenhams-line-generation-algorithm/
/// </summary>
void plotPixel(Image_BW &image, int x1, int y1, int x2, int y2, int dx, int dy, int decide){
  // pk is initial decision making parameter
  // Note:x1&y1,x2&y2, dx&dy values are interchanged
  // and passed in plotPixel function so
  // it can handle both cases when m>1 & m<1
  int pk = 2 * dy - dx;
  for (int i = 0; i <= dx; i++) {
    // checking either to decrement or increment the
    // value if we have to plot from (0,100) to (100,0)
    x1 < x2 ? x1++ : x1--;
    if (pk < 0) {
      // decision value will decide to plot
      // either  x1 or y1 in x's position
      if (decide == 0) {
        // putpixel(x1, y1, RED);
        image[clamp_val(x1, WIDTH)][clamp_val(y1, HEIGHT)] = 1;
        pk = pk + 2 * dy;

      }
      else {
        //(y1,x1) is passed in xt
        // putpixel(y1, x1, YELLOW);
        image[clamp_val(y1, WIDTH)][clamp_val(x1, HEIGHT)] = 1;
        pk = pk + 2 * dy;
      }
    }
    else {
      y1 < y2 ? y1++ : y1--;
      if (decide == 0) {
        image[clamp_val(x1, WIDTH)][clamp_val(y1, HEIGHT)] = 1;

        // putpixel(x1, y1, RED);
      }
      else {
        image[clamp_val(y1, WIDTH)][clamp_val(x1, HEIGHT)] = 1;

        //  putpixel(y1, x1, YELLOW);
      }
      pk = pk + 2 * dy - 2 * dx;
    }
  }
}

/// <summary>
/// Draw line into the image with Bresenham algorithm
/// </summary>
void bresenham(Image_BW &image, int x1, int y1, int x2, int y2){
  int dx = abs(x2 - x1);
  int dy = abs(y2 - y1);
  if (dx > dy) {
    plotPixel(image, x1, y1, x2, y2, dx, dy, 0);
  }
  else {
    plotPixel(image, y1, x1, y2, x2, dy, dx, 1);
  }
}

/// <summary>
/// Rasterizes drawn lines and helplines into the image
/// </summary>
void rasterize_lines(Image_BW &image, Stroke_M lines, std::vector<Line_C> help_lines) {

  Stroke_M::iterator itr;
  for (itr = lines.begin(); itr != lines.end(); ++itr) {
    Line l = itr->second;
    bresenham(image, l.x1, l.y1, l.x2, l.y2);
  }

  std::vector<Line_C>::iterator itr2;
  for (itr2 = help_lines.begin(); itr2 != help_lines.end(); ++itr2) {
    Line_C l = *itr2;
    bresenham(image, l.x1, l.y1, l.x2, l.y2);
  }

}

/// <summary>
/// Checks if pixel [i, j] is still in the canvas
/// </summary>
bool is_in_image(int i, int j) {
  if (i < 0 || i >= WIDTH || j < 0 || j >= HEIGHT)
    return false;
  else
    return true;
}



/// <summary>
///  Go around pixel in certain distance to find a black pixel 
/// Used for creating a contour
/// </summary>
Point find_neib(Image_BW& image, Point p) {
  int line_len = SEGMENTED_STROKE_LEN;
  Point res = p;
  for (int i = p.x - line_len; i <= p.x + line_len; i++) {
    if (is_in_image(i, p.y + line_len) && image[i][p.y + line_len]) {
      res.x = i;
      res.y = p.y + line_len;
      return res;
    }

    if (is_in_image(i, p.y - line_len) && image[i][p.y - line_len]) {
      res.x = i;
      res.y = p.y - line_len;
      return res;
    }
  }
  for (int j = p.y - line_len; j <= p.y + line_len; j++) {
    if (is_in_image(p.x + line_len, j) && image[p.x + line_len][j]) {
      res.x = p.x + line_len;
      res.y = j;
      return res;
    }

    if (is_in_image(p.x - line_len, j) && image[p.x - line_len][j]){
      res.x = p.x - line_len;
      res.y = j;
      return res;
    }
  }
  return p;

}

/// <summary>
/// Euclidean distance between 2 points
/// </summary>
float point_dist(Point p1, Point p2) {
  return sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2));
}

/// <summary>
/// Colors  point p and its surrounding to white
/// </summary>
void delete_suroundings(Image_BW& image, Point p) {
  for (int i = p.x - SEGMENTED_STROKE_LEN; i <= p.x + SEGMENTED_STROKE_LEN; i++) {
    for (int j = p.y - SEGMENTED_STROKE_LEN; j <= p.y + SEGMENTED_STROKE_LEN; j++) {
      if (is_in_image(i, j))
        image[i][j] = 0;
    }
  }
}

/// <summary>
/// Constrols how war away is point p from each line from help_lines structure
/// If point is near to some of the line, returns true
/// </summary>
bool point_is_near_help_line(Point p, std::vector<Line_C>& help_lines) {
  std::vector<Line_C>::iterator itr2;
  bool res = false;
  for (itr2 = help_lines.begin(); itr2 != help_lines.end(); ++itr2) {
    Line_C l = *itr2;
    int max_x = std::max(l.x1, l.x2) + OFFSET;
    int min_x = std::min(l.x1, l.x2) - OFFSET;
    int max_y = std::max(l.y1, l.y2) + OFFSET;
    int min_y = std::min(l.y1, l.y2) - OFFSET;
    // point is inside a box formed by a line l
    if (p.x <= max_x && p.x >= min_x && p.y >= min_y && p.y <= max_y) {

      float dist = abs(l.a * (float)p.x + l.b * (float)p.y + l.c) / sqrt(l.a * l.a + l.b * l.b);

      if (dist <= 2) {
        return true;
      }
    }
  }
  return false;
}

/// <summary>
/// Creates line contour from black-white image
/// </summary>
Stroke_V create_contour_from_BW_image(Image_BW& image, std::vector<Line_C> &help_lines) {
  // find 1. black pixel
  Point end_p;
  end_p.x = end_p.y = 0;
  Stroke_V strokes;

  // find first black pixel
  for (int x = 0; x < WIDTH; x++) {
    for (int y = 0; y < HEIGHT; y++) {
      if (image[x][y]) {
        end_p.x = x;
        end_p.y = y;
        x = WIDTH;
        break;
      }
    }
  }

  //search for the rest of the line
  Point start_p = end_p;
  Point next_p;
  next_p.x = next_p.y = -100;
  int counter = 0;
  while (1) {

    next_p = find_neib(image, start_p);
    if (next_p.x == start_p.x && next_p.y == start_p.y) {
      //cannot find any new lines
      break;
    }

    delete_suroundings(image, start_p);
    //point_is_near_help_line(start_p, help_lines);
    if (!point_is_near_help_line(start_p, help_lines) && !point_is_near_help_line(next_p, help_lines)) {

      Line l;
      l.x1 = start_p.x;
      l.y1 = start_p.y;
      l.x2 = next_p.x;
      l.y2 = next_p.y;
      strokes.push_back(l);
    }
    counter++;
    start_p = next_p;
  }
  
  //connect first and last point
  if (!point_is_near_help_line(start_p, help_lines) && !point_is_near_help_line(next_p, help_lines)) {
    Line l;
    l.x1 = end_p.x;
    l.y1 = end_p.y;
    l.x2 = next_p.x;
    l.y2 = next_p.y;
    strokes.push_back(l);
  }
  return strokes;
}

/// <summary>
/// Filters image, calls contour
/// Saves intermediate results as image files to the computer
/// </summary>
Stroke_V compute_image(Grid* grid, Image_BW &image, std::vector<Line_C> &help_lines, std::vector<Point>& new_area) {
  auto start = std::chrono::high_resolution_clock::now();

  //Image<RGB> output 1, 3  can be deleted, they are here just for showcase
  
  // image after max flow
  Image<RGB> output(WIDTH, HEIGHT);
  // filtered image(area without lines)
  Image<RGB> output2(WIDTH, HEIGHT);
  // segmentation result 
  Image<RGB> output3(WIDTH, HEIGHT);

  for (int y = 0; y < HEIGHT; y++) {
    for (int x = 0; x < WIDTH; x++) {
      output(x, y) = grid->get_segment(grid->node_id(x, y)) ? WHITE : BLACK;
      
    }
  }
 
  //TODO offset pixels on the borders

  // set pixels on the borders to white color
  for (int x = 0; x < OFFSET; x++) {
    for (int y = 0; y < HEIGHT; y++) {
      output2(x, y) = WHITE;
      output2(WIDTH - 1 - x, y) = WHITE;
      output3(x, y) = WHITE;
      output3(WIDTH - 1 - x, y) = WHITE;
      image[x][y] = 0;
      image[WIDTH - 1 - x][y] = 0;

    }
  }

  // set pixels on the borders to white color
  for (int x = 0; x < WIDTH; x++) {
    for (int y = 0; y < OFFSET; y++) {
      output2(x, y) = WHITE;
      output2(x, HEIGHT - 1 - y) = WHITE;
      output3(x, y) = WHITE;
      output3(x, HEIGHT - 1 - y) = WHITE;
      image[x][HEIGHT - 1 - y] = 0;
      image[x][y] = 0;

    }
  }

  // compute black-white area image and line image
  for (int y = OFFSET; y < HEIGHT - OFFSET; y++) {
    for (int x = OFFSET; x < WIDTH - OFFSET; x++) {
      // sum nearby pixels
      float res = 0;
      for (int i = x - OFFSET; i < x + OFFSET; i++) {
        for (int j = y - OFFSET; j < y + OFFSET; j++) {
          res += grid->get_segment(grid->node_id(i, j));
        }
      }
      res /= pow(OFFSET * 2, 2);
      if (res > 0.5) {
        output2(x, y) = WHITE;
      }
      else {
        output2(x, y) = BLACK;
          new_area.push_back(Point(x, y));
      }

      //find borders and color them black
      bool r = output2(x, y) == WHITE ? 1 : 0;
      bool r1 = output2(x - 1, y) == WHITE ? 1 : 0;
      bool r2 = output2(x, y - 1) == WHITE ? 1 : 0;
      if (!r != !r1) {
        image[x][y] = 1;
        output3(x, y) = BLACK;
      }
      else if (!r != !r2) {
        image[x][y] = 1;
        output3(x, y) = BLACK;
      }
      else {
        output3(x, y) = WHITE;
        image[x][y] = 0;

      }
    }
  }

  //-------- create lines from the BW_image -----
  Stroke_V strokes = create_contour_from_BW_image(image, help_lines);
 
  auto stop = std::chrono::high_resolution_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
  std::cout << "computiong area in " << duration.count() << "\n";


  srand((unsigned)time(0));
  std::string name = "output1_";
  std::string name2 = "output2_";
  std::string name3 = "output3_";
  std::string end = std::to_string((rand() % 8)) + ".png";
  // to see intermediate results, uncomment this part
  /*
    imwrite(output, name + end);
    imwrite(output2, name2 + end);
    imwrite(output3, name3 + end);
  */
  return strokes;
}

/// <summary>
/// Computes weight between two colors of pixels
/// </summary>
short weight_func(bool pixl1, bool pixl2) {
  short cap;
  //pixels are diffrent
  if ((pixl1 && !pixl2) || (!pixl1 && pixl2)) {
    cap = 1;
  }
  else {
    cap = CAP;
  }
  return cap;
}

/// <summary>
/// Sets the parametrs for the graph, computes max-flow and then computes the final image
/// </summary>
Stroke_V segment_drawing(Stroke_M lines, std::vector<Line_C> &help_lines, std::vector<Point> &new_area) {
  auto start = std::chrono::high_resolution_clock::now();

  Grid* grid = new Grid(WIDTH, HEIGHT);
  Image_BW image(WIDTH, std::vector<bool>(HEIGHT, 0));
  rasterize_lines(image, lines, help_lines);


  auto stop = std::chrono::high_resolution_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
  std::cout << "rasterization in " << duration.count() << "\n";
 
  //longest lasting part
  for (int x = 0; x < WIDTH; x++){
    for (int y = 0; y < HEIGHT; y++){
      
        //image[x][y]  == 1 -> black. 
        grid->set_terminal_cap(grid->node_id(x, y), image[x][y] ? K : 0, 0);

        if (x < WIDTH - 1){
          
          short cap = weight_func(image[x][y], image[x+1][y]);

          grid->set_neighbor_cap(grid->node_id(x, y), +1, 0, cap);
          grid->set_neighbor_cap(grid->node_id(x + 1, y), -1, 0, cap);
        } else {
          // borders
          grid->set_terminal_cap(grid->node_id(x, y), 0, K);
        }


        if (y < HEIGHT - 1){

          short cap = weight_func(image[x][y], image[x][y + 1]);
          
          grid->set_neighbor_cap(grid->node_id(x, y), 0, +1, cap);
          grid->set_neighbor_cap(grid->node_id(x, y + 1), 0, -1, cap);
        }
        else {
          // borders
          grid->set_terminal_cap(grid->node_id(x, y), 0, K);
         }
        if (y == 0 || x == 0) {
          // borders
          grid->set_terminal_cap(grid->node_id(x, y), 0, K);
        }
       
      }

    }

  start = std::chrono::high_resolution_clock::now();
  duration = std::chrono::duration_cast<std::chrono::milliseconds>(start - stop);
  std::cout << "setting parameters in " << duration.count() << "\n";

  grid->compute_maxflow();

  stop = std::chrono::high_resolution_clock::now();
  duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop-start);
  std::cout << "computing max flow in " << duration.count() << "\n";

  Stroke_V strokes = compute_image(grid, image, help_lines, new_area);

  stop = std::chrono::high_resolution_clock::now();
  duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
  std::cout << "writing to file in " << duration.count() << "\n";
 

  delete grid;
  return strokes;
}

