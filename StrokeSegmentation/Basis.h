#pragma once
#include <vector>
#include <map>

#define KEY_SEEN     1
#define KEY_RELEASED 2
#define FPS 30
#define WIDTH 800
#define HEIGHT 600

#define MIN_LINE_LEN 3.0
//stroke vector and stroke map
#define Stroke_V std::vector<Line>
#define Stroke_M std::map<int, Line>

//-----------------------------------

#define SEGMENTED_STROKE_LEN 3

#define LINE_WIDTH 3
// for clearing lines from area image
#define OFFSET 3
//------------------------------------


#define WEIGHT(A) (short)(1+K*std::exp((-(A)*(A)/SIGMA2)))
#define Image_BW std::vector<std::vector<bool>> 

//testing
#include <chrono>


struct Line {
  int x1;
  int x2;
  int y1;
  int y2;
};

//line with coeficients
struct Line_C {
  int x1;
  int x2;
  int y1;
  int y2;
  float a;
  float b;
  float c;
};

class Point {
public:
  int x;
  int y;
  Point() {
	x = 0;
	y = 0;
  }
  Point(int x1, int y2) {
	x = x1;
	y = y2;
  }
};

/// <summary>
/// Clamps values exceeding borders of the canvas
/// </summary>
int clamp_val(int x, int size);

