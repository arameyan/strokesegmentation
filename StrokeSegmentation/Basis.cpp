

int clamp_val(int x, int size) {
  if (x < 0)
    return 0;

  if (x >= size)
    return size - 1;

  return x;
}